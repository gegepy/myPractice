package com.peng.basic;

import java.util.Arrays;

/**
 * 计算给定字符串中各字母出现次数
 * 1. 可以使用 Map 存储
 * 2. 可以使用数组存储
 *
 * @author Pengyan
 * @date 2019/3/25 14:29
 **/
public class CharacterCounter {

    /**
     * 使用数组存储次数，这里的实现只考虑了给定字符串中只有小写字母，不存在其他符号，否则将出现数组越界
     *
     * @param args
     */
    public static void main(String[] args) {
        String targetString = "nfajfieuojosvoricxjiojafaewqtyruioplkjhgasdfvbxzcnm";
        // 字母数组长度
        final int charSize = 26;
        // 字母 a ascii码值
        final int char_a_ascii = 97;
        int[] arrs = new int[charSize];
        char[] chars = new char[charSize];
        // 利用ascii码值可以作为数组下标实现
        for (int i = 0; i < targetString.length(); i++) {
            arrs[targetString.charAt(i) - char_a_ascii]++;
        }
        System.out.println(Arrays.toString(arrs));

        for (int i = 0; i < charSize; i++) {
            chars[i] = (char) (i + char_a_ascii);
        }
        System.out.println(Arrays.toString(chars));
    }
}
