package com.peng.basic;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Objects;

/**
 * 正确重写对象的 equals 方法及 hashCode 方法（hashMap中对象用作key等情况时，强制必须重写这两个方法）
 *
 * @author <a href="mailto:pengyan@loonxi.com">pengyan</a>
 * @date 2018/8/2 9:03
 **/
@Getter
@Setter
@Slf4j
public class OverrideEqualsAndHashcode {

    private String name;
    private int age;

    public OverrideEqualsAndHashcode(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(obj == null || !(obj instanceof OverrideEqualsAndHashcode)) {
            return false;
        }
        OverrideEqualsAndHashcode target = (OverrideEqualsAndHashcode) obj;
        return age == target.age && Objects.equals(name, target.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    /**
     * hashCode2 hashCode3 方法也是一种实现，不过其原理都差不多
     * @return
     */
    public int hashCode2() {
        int result = 17;
        result = 31 * result + name == null ? 0 : name.hashCode();
        result = 31 * result + age;
        return result;
    }

    public int hashCode3() {
        return new HashCodeBuilder().append(name).append(age).toHashCode();
    }

    public static void main(String[] args) {
        OverrideEqualsAndHashcode obj1 = new OverrideEqualsAndHashcode(null, 30);
        OverrideEqualsAndHashcode obj2 = new OverrideEqualsAndHashcode(null, 30);
        OverrideEqualsAndHashcode obj3 = new OverrideEqualsAndHashcode("张三", 30);
        log.info("obj1.equals(obj2) result: {}", obj1.equals(obj2));
        log.info("obj1.equals(obj3) result: {}", obj1.equals(obj3));
    }

}
