package com.peng.utils;

import java.util.Stack;

/**
 * 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串，判断字符串是否有效。
 *
 *      左括号必须用相同类型的右括号闭合
 *      左括号必须以正确的顺序闭合
 *      注意空字符串可被认为是有效字符串
 *
 *      （其它更复杂的验证需要自己扩展）
 * @author Pengyan
 * @date 2018/12/8 14:12
 **/
public class BracketsValidator {

    public static void main(String[] args) {
        String target = " {  }[{({})}]()";
        System.out.println(isValid(target));
    }

    private static boolean isValid(String s) {
        if (s == null) {
            throw new RuntimeException("字符串不能为null");
        }
        s = s.replaceAll("\\s*", "");
        if (s.length() == 0) {
            return true;
        }
        Stack stack = new Stack();
        String left = "{[(";
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char currentChar = chars[i];
            if (left.indexOf(currentChar) != -1) {
                stack.push(currentChar);
            } else if (stack.empty()) {
                return false;
            } else {
                char c = (char) stack.pop();
                if (c == '(' && currentChar != ')') {
                    return false;
                }
                if (c == '{' && currentChar != '}') {
                    return false;
                }
                if (c == '[' && currentChar != ']') {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean isValid2(String s) {
        if (s == null) {
            throw new RuntimeException("字符串不能为null");
        }
        s = s.replaceAll("\\s*", "");
        while (s.contains("{}") || s.contains("[]") || s.contains("()")) {
            s = s.replace("{}", "").replace("[]", "").replace("()", "");
        }
        if (s.length() == 0) {
            return true;
        }
        return false;
    }
}
