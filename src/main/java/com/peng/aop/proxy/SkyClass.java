package com.peng.aop.proxy;

/**
 * @Auther: pengyan
 * @Date: 2018/7/5 18:16
 * @Description:
 */
public class SkyClass implements SkyInterface {

    @Override
    public void rain() {
        System.out.println("开始下雨啦~");
    }
}
