package com.peng.aop.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @Auther: pengyan
 * @Date: 2018/7/5 18:18
 * @Description:
 */
public class MyInvocationHandler implements InvocationHandler {

    // 目标对象
    private Object target;

    public Object getProxy(Object target) {
        this.target = target;
        return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("下雨之前会打雷吧");
        Object result = method.invoke(target, args);
        System.out.println("雨下完啦~天晴了");
        return result;
    }
}
