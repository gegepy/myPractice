package com.peng.aop.proxy;

/**
 * @Auther: pengyan
 * @Date: 2018/7/5 18:26
 * @Description:
 */
public class ProxyTest {

    public static void main(String[] args) {
        MyInvocationHandler myInvocationHandler = new MyInvocationHandler();
        SkyInterface skyInterface = (SkyInterface)myInvocationHandler.getProxy(new SkyClass());
        skyInterface.rain();
    }
}
