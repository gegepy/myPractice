package com.peng.aop.cglib;


/**
 * @Auther: pengyan
 * @Date: 2018/7/5 18:35
 * @Description: 基于cglib的切面实现
 */
public class ProxyTest {

    public static void main(String[] args) {
        MyCglib myCglib = new MyCglib();
        SkyCalss skyCalss = (SkyCalss)myCglib.getProxy(new SkyCalss());
        skyCalss.rain();
    }
}
