package com.peng.aop.cglib;

/**
 * @Auther: pengyan
 * @Date: 2018/7/5 18:29
 * @Description: 这个类不能为final修饰
 */
public class SkyCalss {

    public void rain() {
        System.out.println("开始下雨啦~");
    }
}
