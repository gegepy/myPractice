package com.peng.aop.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @Auther: pengyan
 * @Date: 2018/7/5 18:30
 * @Description:
 */
public class MyCglib implements MethodInterceptor {

    // 目标对象
    private Object target;

    public Object getProxy(Object target) {
        this.target = target;
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(target.getClass());
        enhancer.setCallback(this);
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("下雨之前会打雷吧");
        Object result = method.invoke(target, objects);
        System.out.println("雨下完啦~天晴了");
        return result;
    }
}
