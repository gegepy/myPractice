package com.peng.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * 开启三个线程，分别打印字母 A、B、C
 * 要求按 ABC 顺序打印 10 遍
 */
public class PrintABC {

    private final int TIMES = 10;

    private Semaphore semaphoreA = new Semaphore(1),
                      semaphoreB = new Semaphore(0),
                      semaphoreC = new Semaphore(0);


    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        PrintABC print = new PrintABC();
        executorService.execute(print.new ThreadA());
        executorService.execute(print.new ThreadB());
        executorService.execute(print.new ThreadC());
        executorService.shutdown();
    }

    private class ThreadA implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < TIMES; i++) {
                try {
                    semaphoreA.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("A");
                semaphoreB.release();
            }
        }
    }
    private class ThreadB implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < TIMES; i++) {
                try {
                    semaphoreB.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("B");
                semaphoreC.release();
            }
        }
    }
    private class ThreadC implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < TIMES; i++) {
                try {
                    semaphoreC.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("C");
                semaphoreA.release();
            }
        }
    }
}
