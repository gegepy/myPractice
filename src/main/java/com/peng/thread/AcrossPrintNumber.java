package com.peng.thread;

/**
 * 两个线程交替打印奇偶数
 *
 * 结果：
 * 奇数线程:1
 * 偶数线程:2
 * 奇数线程:3
 * 偶数线程:4
 *
 * @author Pengyan
 * @date 2019/3/14 15:51
 **/
public class AcrossPrintNumber implements Runnable {

    static Integer i = 1;

    @Override
    public void run() {
        while (i < 100) {
            synchronized (AcrossPrintNumber.class) {
                System.out.println(Thread.currentThread().getName() + ":" + i++);
                AcrossPrintNumber.class.notify();
                try {
                    AcrossPrintNumber.class.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        new Thread(new AcrossPrintNumber(), "奇数线程").start();
        new Thread(new AcrossPrintNumber(), "偶数线程").start();
    }
}
