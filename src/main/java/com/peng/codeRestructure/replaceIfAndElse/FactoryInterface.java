package com.peng.codeRestructure.replaceIfAndElse;

/**
 * @Auther: pengyan
 * @Date: 2018/6/27 16:18
 * @Description:
 */
@FunctionalInterface
public interface FactoryInterface {
    void methodExecutor(MethodProvide methodProvide, Entity entity);
}
