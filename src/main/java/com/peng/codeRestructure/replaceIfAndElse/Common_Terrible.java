package com.peng.codeRestructure.replaceIfAndElse;

/**
 * @Auther: pengyan
 * @Date: 2018/6/27 15:42
 * @Description: 普通处理，if/else ，一种常见而难看的处理方式
 */
public class Common_Terrible {


    public static void main(String[] args) {
        Entity entity = new Entity(2);

        MethodProvide method = new MethodProvide();
        if(1 == entity.getCode()) {
            method.setFirstStage(entity);
        } else if(2 == entity.getCode()) {
            method.setSecondStage(entity);
        } else if(3 == entity.getCode()) {
            method.setThirdStage(entity);
        } else if(4 == entity.getCode()) {
            method.setFourthStage(entity);
        } else if(5 == entity.getCode()) {
            method.setFifthStage(entity);
        }
        System.out.println(entity.getName());
    }
}
