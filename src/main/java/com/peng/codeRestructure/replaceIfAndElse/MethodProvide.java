package com.peng.codeRestructure.replaceIfAndElse;

/**
 * @Auther: pengyan
 * @Date: 2018/6/27 15:46
 * @Description:
 */
public class MethodProvide {

    public void setFirstStage(Entity entity) {
        entity.setName("设置了它的名称为1");
    }

    public void setSecondStage(Entity entity) {
        entity.setName("设置了它的名称为2");
    }

    public void setThirdStage(Entity entity) {
        entity.setName("设置了它的名称为3");
    }

    public void setFourthStage(Entity entity) {
        entity.setName("设置了它的名称为4");
    }

    public void setFifthStage(Entity entity) {
        entity.setName("设置了它的名称为5");
    }
}
