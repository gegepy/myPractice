package com.peng.codeRestructure.replaceIfAndElse;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author: pengyan
 * @date: 2018/6/27 15:52
 * @description: 通过反射实现，稍微好看点，但是反射效率很低
 */
public class Reflect_LookGood {

    private static Map<Integer, String> methodsMap = new HashMap<>();

    static {
        methodsMap.put(1, "setFirstStage");
        methodsMap.put(2, "setSecondStage");
        methodsMap.put(3, "setThirdStage");
        methodsMap.put(4, "setFourthStage");
        methodsMap.put(5, "setFifthStage");
    }

    public static void main(String[] args) {
        Entity entity = new Entity(3);
        MethodProvide methodProvide = new MethodProvide();

        try {
            String methodName = methodsMap.get(entity.getCode());
            Method method = MethodProvide.class.getMethod(methodName, entity.getClass());
            method.invoke(methodProvide, entity);
            System.out.println(entity.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
