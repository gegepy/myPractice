package com.peng.codeRestructure.replaceIfAndElse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: pengyan
 * @Date: 2018/6/27 16:16
 * @Description: 策略模式实现
 */
public class Factory_Better {

    private static Map<Integer, FactoryInterface> methodMap = new HashMap<Integer, FactoryInterface>(8) {
        {
            put(1, (method, entity)-> method.setFirstStage(entity));
            put(2, (method, entity)-> method.setSecondStage(entity));
            put(3, (method, entity)-> method.setThirdStage(entity));
            put(4, (method, entity)-> method.setFourthStage(entity));
            this.put(5, (method, entity)-> method.setFifthStage(entity));
        }
    };

    public static void main(String[] args) {
        Entity entity = new Entity(5);
        MethodProvide methodProvide = new MethodProvide();

        FactoryInterface factoryInterface = methodMap.get(entity.getCode());
        factoryInterface.methodExecutor(methodProvide, entity);
        System.out.println(entity.getName());
    }
}
