package com.peng.codeRestructure.replaceIfAndElse;

/**
 * @Auther: pengyan
 * @Date: 2018/6/27 15:38
 * @Description:
 */
public class Entity {

    private int code;
    private String name;

    public Entity(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
