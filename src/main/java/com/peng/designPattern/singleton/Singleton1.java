package com.peng.designPattern.singleton;

/**
 * 单例模式——懒汉式<br>
 * 懒汉式：只有当调用getInstance的时候，才回去初始化这个单例
 * @author <a href="mailto:pengyan@loonxi.com">pengyan</a>
 * @date 2018/7/16 20:55
 **/
public class Singleton1 {

    private static Singleton1 singleton = null;

    /**
     * 私有化构造方法，防止外部实例化（当然，通过反射还是能实现实例化的）
     */
    private Singleton1() {
    }

    /**
     * 线程不安全的
     * @return
     */
    public static Singleton1 getInstance() {
        if(singleton == null) {
            singleton = new Singleton1();
        }
        return singleton;
    }

    /**
     * 线程安全，但是每次都要同步，大部分都是浪费
     * @return
     */
    public static synchronized Singleton1 getInstance2() {
        if(singleton == null) {
            singleton = new Singleton1();
        }
        return singleton;
    }

    /**
     * 双重检查锁定
     * 线程安全，做了两次null检查，确保了只有第一次调用单例的时候才会做同步，避免了每次都同步的性能损耗
     * @return
     */
    public static Singleton1 getInstance3() {
        if(singleton == null) {
            synchronized (Singleton1.class) {
                if(singleton == null) {
                    singleton = new Singleton1();
                }
            }
        }
        return singleton;
    }
}
