package com.peng.designPattern.singleton;

/**
 * 单例模式——饿汉式<br>
 * 在类创建的同时就已经创建好一个静态的对象供系统使用，以后不再改变，所以天生是线程安全的
 * @author <a href="mailto:pengyan@loonxi.com">pengyan</a>
 * @date 2018/7/17 16:12
 **/
public class Singleton3 {

    private static Singleton3 singleton = new Singleton3();

    private Singleton3() {
    }

    public static Singleton3 getInstance() {
        return singleton;
    }
}
