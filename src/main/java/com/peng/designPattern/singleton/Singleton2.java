package com.peng.designPattern.singleton;

/**
 * 单例模式——懒汉式2<br>
 * 通过静态内部类实现，利用了classloader的机制来保证初始化instance时只有一个线程，所以也是线程安全的，同时没有性能损耗
 * @author <a href="mailto:pengyan@loonxi.com">pengyan</a>
 * @date 2018/7/16 21:05
 **/
public class Singleton2 {

    /**
     * 私有化构造方法，防止外部实例化
     */
    private Singleton2() {
    }

    private static class Lazyloader {
        private static Singleton2 singleton = new Singleton2();
    }

    public static Singleton2 getInstance() {
        return Lazyloader.singleton;
    }
}
