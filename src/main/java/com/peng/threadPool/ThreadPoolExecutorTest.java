package com.peng.threadPool;

import java.util.concurrent.*;

/**
 * ThreadPoolExecutor 线程池测试
 *
 *   1、线程池刚创建时，里面没有一个线程。任务队列是作为参数传进来的。不过，就算队列里面有任务，线程池也不会马上执行它们。
 *   2、当调用 execute() 方法添加一个任务时，线程池会做如下判断：
 *   　　 a. 如果正在运行的线程数量小于 corePoolSize，那么马上创建线程运行这个任务；
 *   　　 b. 如果正在运行的线程数量大于或等于 corePoolSize，那么将这个任务放入队列。
 *   　　 c. 如果这时候队列满了，而且正在运行的线程数量小于 maximumPoolSize，那么还是要创建线程运行这个任务；
 *   　　 d. 如果队列满了，而且正在运行的线程数量大于或等于 maximumPoolSize，那么线程池会抛出异常，告诉调用者“我不能再接受任务了”。
 *   3、当一个线程完成任务时，它会从队列中取下一个任务来执行。
 *   4、当一个线程无事可做，超过一定的时间（keepAliveTime）时，线程池会判断，如果当前运行的线程数大于 corePoolSize，那么这个线程就被停掉。所以线程池的所有任务完成后，它最终会收缩到corePoolSize 的大小。
 *   这样的过程说明，并不是先加入任务就一定会先执行。假设队列大小为10，corePoolSize 为 3，maximumPoolSize 为 6，那么当加入 20 个任务时，执行的顺序就是这样的：首先执行任务 1、2、3，然后任务 4~13 被放入队列。这时候队列满了，任务 14、15、16 会被马上执行，而任务 17~20 则会抛出异常。最终顺序是：1、2、3、14、15、16、4、5、6、7、8、9、10、11、12、13。
 *
 * @author Pengyan
 * @date 2018/12/7 18:12
 **/
public class ThreadPoolExecutorTest {

    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>(10);
        // 这里设置 keepAliveTime 为 3 秒，后面测试用
        ThreadPoolExecutor executor = new ThreadPoolExecutor(3, 6, 3, TimeUnit.SECONDS, queue,
                (r, executor1) -> System.err.println(String.format("Task %d rejected.", r.hashCode())));

        System.err.println("线程池大小——初始化："+executor.getPoolSize());
        // 实际只有16个任务执行
        CountDownLatch countDownLatch = new CountDownLatch(16);
        for (int i = 0; i < 20; i++) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                        System.err.println(String.format("thread %d finished", this.hashCode()));
                        countDownLatch.countDown();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        countDownLatch.await();
        System.err.println("线程池大小——线程全部执行完成："+executor.getPoolSize());
        Thread.sleep(2000);
        System.err.println("线程池大小——等待2秒后："+executor.getPoolSize());
        Thread.sleep(2000);
        System.err.println("线程池大小——等待4秒后："+executor.getPoolSize());

        executor.shutdown();
    }
}
