package com.peng.collection;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 按对象的指定属性移除重复项
 *
 * @author Pengyan
 * @date 2018/8/21 9:57
 **/
public class RemoveListDuplicateObject {

    public static void main(String[] args) {
        List<Car> list = new ArrayList<Car>(6) {{
            add(new Car("110", "red", "Land Rover"));
            add(new Car("110", "green", "Ford"));
            add(new Car("111", "blue", "Porsche"));
            add(new Car("112", "black", "Volvo"));
            add(new Car("112", "purple", "Cadillac"));
            add(new Car("113", "gray", "Benz"));
        }};

        System.out.println("去重前：");
        list.forEach(System.out::println);

       /* list = removeDuplicateCar(list);
        System.out.println("去重后：");
        list.forEach(System.out::println);*/

       // java8 中更简约的实现
        List<Car> newList = list.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(Car::getNo))), ArrayList::new));
        System.out.println("去重后：");
        newList.forEach(System.out::println);
    }

    private static ArrayList<Car> removeDuplicateCar(List<Car> cars) {
        // 按 no 去重
        //字符串,则按照asicc码升序排列
        Set<Car> set = new TreeSet(Comparator.comparing(Car::getNo));
        set.addAll(cars);
        return new ArrayList<Car>(set);
    }

    @Getter
    @Setter
    static class Car {
        private String no;
        private String color;
        private String brand;

        public Car(String no, String color, String brand) {
            Assert.notNull(no);
            this.no = no;
            this.color = color;
            this.brand = brand;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }
}
