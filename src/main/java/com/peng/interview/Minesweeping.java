package com.peng.interview;

import java.util.Arrays;
import java.util.Random;

/**
 * 生成扫雷游戏棋盘
 */
public class Minesweeping {

    /** 边长 n*n的盘局 */
    private static final int SIDE_LENTH = 5;
    /** 地雷数 */
    private static final int MINE_AMOOUNT = 10;
    /** 地雷的标识 */
    private static final String MINE_CHAR = "#";

    public static void main(String[] args) {
        String[][] array = new String[SIDE_LENTH][SIDE_LENTH];
        for (int i = 0; i < MINE_AMOOUNT; i++) {
            boolean created = false;
            while (!created) {
                int x = getRandom(SIDE_LENTH), y = getRandom(SIDE_LENTH);
                if (array[x][y] == null) {
                    array[x][y] = MINE_CHAR;
                    created = true;
                }
            }
        }

        for (int i = 0; i < SIDE_LENTH; i++) {
            for (int j = 0; j < SIDE_LENTH; j++) {
                if (array[i][j] == null) {
                    array[i][j] = countAround(i, j, array);
                }
            }
        }

        for (int i = 0; i < SIDE_LENTH; i++) {
            String[] row = array[i];
            System.out.println(Arrays.toString(row));
        }
    }

    /**
     * 生成随机数，范围 [0, maxValue)
     * @param maxValue
     * @return
     */
    private static int getRandom(int maxValue) {
        Random random = new Random();
        return random.nextInt(maxValue);
    }

    /**
     * 统计周围地雷数
     * @param x
     * @param y
     * @param array
     * @return
     */
    private static String countAround(int x, int y, String[][] array) {
        int i = 0;

        if (x>0 && y>0 && MINE_CHAR.equals(array[x-1][y-1])) {
            i++;
        }
        if (x>0 && y != SIDE_LENTH-1 && MINE_CHAR.equals(array[x-1][y+1])) {
            i++;
        }
        if (x>0 && MINE_CHAR.equals(array[x-1][y])) {
            i++;
        }
        if (y != SIDE_LENTH-1 && MINE_CHAR.equals(array[x][y+1])) {
            i++;
        }
        if (y > 0 && MINE_CHAR.equals(array[x][y-1])) {
            i++;
        }
        if (x != SIDE_LENTH-1 && y>0 && MINE_CHAR.equals(array[x+1][y-1])) {
            i++;
        }
        if (x != SIDE_LENTH-1 && y != SIDE_LENTH-1 && MINE_CHAR.equals(array[x+1][y+1])) {
            i++;
        }
        if (x != SIDE_LENTH-1 && MINE_CHAR.equals(array[x+1][y])) {
            i++;
        }
        return String.valueOf(i);
    }
}
