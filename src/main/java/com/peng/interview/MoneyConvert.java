package com.peng.interview;

/**
 * 金钱数字转换为大写
 */
public class MoneyConvert {

    private static final double target = 29710013.124;

    private static final String[] NUM = new String[]{"零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"};

    private static final String[] scale = new String[]{"十", "佰", "仟", "万", "十", "佰", "仟", "亿"};

    public static void main(String[] args) {
        // 特别注意，"." 和 "|" 为特殊字符，需要进行转义
//        String[] money = String.valueOf(target).split("\\.");
        String[] money = String.format("%.2f",target).split("\\.");
        char[] nums = money[0].toCharArray();
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < nums.length; i++) {
            str.append(NUM[nums[i] - '0']);
            str.append(scale[7-i]);
        }
        System.out.println(str);
    }
}
