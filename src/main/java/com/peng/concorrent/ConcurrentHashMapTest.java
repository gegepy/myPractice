package com.peng.concorrent;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 问题：后台逻辑部分，如何快速获取并处理100w条映射列表相关的数据？
 *
 * @author <a href="mailto:pengyan@loonxi.com">pengyan</a>
 * @date 2018/7/27 14:36
 **/
public class ConcurrentHashMapTest {

    static final private int TIMES = 1000000;
    /**
     * 线程数
     *  如果是IO密集型应用，则线程池大小设置为2N+1；
     *  如果是CPU密集型应用，则线程池大小设置为N+1；
     */
    private final static int THREAD_AMOUNT = Runtime.getRuntime().availableProcessors() * 2;
    private static ConcurrentHashMap<Integer, String> map = new ConcurrentHashMap<>(TIMES);
    private static AtomicInteger at = new AtomicInteger(0);

    public static void main(String[] args) {
        for (int i = 0;i < TIMES; i++) {
            map.put(i, "元素"+i);
        }
        System.out.println("----100w条数据加载完毕，开始执行-----");
        long stime = System.currentTimeMillis();
        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_AMOUNT);
        for (int i = 0;i < TIMES; i++) {
            executorService.execute(() -> {
                System.out.println(map.get(at.getAndIncrement()));
            });
        }
        executorService.shutdown();

        while (true) {
            if(executorService.isTerminated()) {
                long etime = System.currentTimeMillis();
                System.out.println("----100w条数据获取完毕，执行时间："+(etime-stime));
                break;
            }
        }
    }
}
