package com.peng.concorrent.synchr;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * synchronized修饰类或静态方法时，作用于所有对象。同一时刻只有一个线程执行
 *
 * @author <a href="mailto:pengyan@loonxi.com">pengyan</a>
 * @date 2018/8/2 19:38
 **/
@Slf4j
public class SynchronizedStaticTest {

    /**
     * 修饰类
     * @param j
     */
    private void test1(int j) {
        synchronized (SynchronizedStaticTest.class) {
            for (int i = 0; i < 10; i++) {
                log.info("test1: {} - {}", j, i);
            }
        }
    }

    /**
     * 修饰静态方法
     * @param j
     */
    private static synchronized void test2(int j) {
        for (int i = 0; i < 10; i++) {
            log.info("test2: {} - {}", j, i);
        }
    }

    public static void main(String[] args) {
        SynchronizedStaticTest e1 = new SynchronizedStaticTest();
        SynchronizedStaticTest e2 = new SynchronizedStaticTest();
        ExecutorService executor = Executors.newCachedThreadPool();

        executor.execute(() -> {
            e1.test1(1);
        });
        executor.execute(() -> {
            e2.test1(2);
        });
        executor.shutdown();
    }
}
