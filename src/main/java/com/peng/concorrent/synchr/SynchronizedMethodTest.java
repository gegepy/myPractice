package com.peng.concorrent.synchr;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * synchronized修饰代码块或普通方法时，作用于调用的对象。不同对象调用相互之间不影响
 *
 * @author <a href="mailto:pengyan@loonxi.com">pengyan</a>
 * @date 2018/8/2 19:28
 **/
@Slf4j
public class SynchronizedMethodTest {

    /**
     * 修饰代码块
     * @param j
     */
    private void test1(int j) {
        synchronized (this) {
            for (int i = 0; i < 10; i++) {
                log.info("test1: {} - {}", j, i);
            }
        }
    }

    /**
     * 修饰方法
     * @param j
     */
    private synchronized void test2(int j) {
        for (int i = 0; i < 10; i++) {
            log.info("test2: {} - {}", j, i);
        }
    }

    public static void main(String[] args) {
        SynchronizedMethodTest e1 = new SynchronizedMethodTest();
        SynchronizedMethodTest e2 = new SynchronizedMethodTest();
        ExecutorService executor = Executors.newCachedThreadPool();
        /*
        同一个对象会被加锁
        executor.execute(() -> {
            e1.test1(1);
        });
        executor.execute(() -> {
            e1.test1(1);
        });*/

        // 不同对象之间互不影响
        executor.execute(() -> {
            e1.test2(1);
        });
        executor.execute(() -> {
            e2.test2(2);
        });
        executor.shutdown();
    }
}
