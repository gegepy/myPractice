package com.peng.concorrent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 * @Auther: pengyan
 * @Date: 2018/6/13 10:46
 * @Description: CyclicBarrier 线程间相互等待，所有完成后继续执行接下来的操作
 */
public class CyclicBarrierTest {

    private static Logger log = LoggerFactory.getLogger(CyclicBarrierTest.class);

    static class Writer extends Thread {
        private CyclicBarrier cyclicBarrier;

        private int times;

        public Writer(CyclicBarrier cyclicBarrier, int times) {
            this.cyclicBarrier = cyclicBarrier;
            this.times = times;
        }

        @Override
        public void run() {
            log.info("线程"+Thread.currentThread().getName()+"正在写入数据...");
            try {
                long sleepMills = 0;
                switch (times) {
                    case 0 :
                        sleepMills = 1000;// 第一个任务执行1000毫秒
                        break;
                    case 1 :
                        sleepMills = 1400;// 第二个任务执行1400毫秒，和第一个差400毫秒，不会报错
                        break;
                    case 2 :
                        sleepMills = 1401;// 第二个任务执行1401毫秒，和第一个差401毫秒，导致异常抛出
                        break;
                    case 3 :
                        sleepMills = 1200;
                        break;
                }
                Thread.sleep(sleepMills);
                log.info("线程"+Thread.currentThread().getName()+"经过"+sleepMills+"毫秒执行完成，等待其他线程完成");

                cyclicBarrier.await(400, TimeUnit.MILLISECONDS);// 这里设置超时时差为400毫秒

                System.out.println("还执行不？");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public static void main(String[] args) {
            int threadSum = 4;
            CyclicBarrier barrier = new CyclicBarrier(threadSum);

            for (int i=0;i<threadSum;i++) {
                new Writer(barrier, i).start();
            }
        }
    }
}
