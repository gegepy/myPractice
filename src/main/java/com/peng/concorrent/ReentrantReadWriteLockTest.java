package com.peng.concorrent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 可重入读写锁实例
 *
 * @author Pengyan
 * @date 2019/3/7 20:55
 **/
public class ReentrantReadWriteLockTest {

    private Map<String, Data> map = new HashMap<>();

    private ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
    private Lock wLock = rwLock.writeLock();
    private Lock rLock = rwLock.readLock();

    /** 获取使用读锁 */
    public Data get(String key) {
        rLock.lock();
        try {
            return map.get(key);
        } finally {
            rLock.unlock();
        }
    }

    /** 写入使用写锁 */
    public Data put(String key, Data data) {
        wLock.lock();
        try {
            return map.put(key, data);
        } finally {
            wLock.unlock();
        }
    }

    /** 内部类。内容对象 */
    public class Data {
        private Integer num;

        public Integer getNum() {
            return num;
        }

        public void setNum(Integer num) {
            this.num = num;
        }

        public Data(Integer num) {
            this.num = num;
        }

        @Override
        public String toString() {
            return "值为：" + num;
        }
    }

    // #############################################

    private static final int threads = 10;

    private static final int amount = 1000;

    public static void main(String[] args) throws InterruptedException {
        ReentrantReadWriteLockTest rl = new ReentrantReadWriteLockTest();
        /*Data data1 = rl.new Data(1);
        Data data2 = rl.new Data(3);
        Data data3 = rl.new Data(5);
        rl.put("1", data1);
        rl.put("2", data2);
        rl.put("3", data3);
        System.out.println(rl.get("2"));*/

        ExecutorService executorService = Executors.newFixedThreadPool(threads);
        for (int i = 0;i < amount; i++) {
            final int j = i;
            executorService.execute(() -> {
                if (j % 5 == 0) {
                    rl.get(String.valueOf(j));
                } else {
                    rl.put(String.valueOf(j), rl.new Data(j));
                }
            });
        }

        executorService.shutdown();
        while (!executorService.isTerminated()) {
            Thread.sleep(1000);
        }
        // 加了锁 map 的大小为800，不加锁可能小于800
        System.out.println(rl.map.size());
    }

}
