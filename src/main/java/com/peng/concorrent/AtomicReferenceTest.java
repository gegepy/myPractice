package com.peng.concorrent;

import java.util.concurrent.atomic.AtomicReference;

/**
 * 无锁的对象引用
 *
 * @author <a href="mailto:pengyan@loonxi.com">pengyan</a>
 * @date 2018/8/1 20:23
 **/
public class AtomicReferenceTest {

    public static void main(String[] args) {
        AtomicReference auto = new AtomicReference(0);
        // 如果是0时将值设置为2
        auto.compareAndSet(0,2);// 2
        auto.compareAndSet(0,1);// no
        auto.compareAndSet(2,3);// 3
        auto.compareAndSet(2,5);// no
        auto.compareAndSet(1,8);// no
        System.out.println(auto.get());
    }
}
