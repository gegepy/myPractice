package com.peng.concorrent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.concurrent.CountDownLatch;

/**
 * @Auther: pengyan
 * @Date: 2018/6/13 11:37
 * @Description:
 */
public class CountDownLatchTest {

    private static Logger log = LoggerFactory.getLogger(CountDownLatchTest.class);
    
    static class Writer extends Thread {
        private CountDownLatch countDownLatch;

        public Writer(CountDownLatch countDownLatch) {
            this.countDownLatch = countDownLatch;
        }

        @Override
        public void run() {
            log.info("线程"+Thread.currentThread().getName()+"正在执行中...");
            try {
                Random ra =new Random();
                long sleepMills = ra.nextInt(1000)+1000;
                Thread.sleep(sleepMills);
                log.info("线程"+Thread.currentThread().getName()+"经过"+sleepMills+"毫秒"+"执行完成，接着做下面的事啦");

                countDownLatch.countDown();

                log.info(Thread.currentThread().getName()+"我下面要做的事开始！这件事就不需要主线程等待啦~");
                Thread.sleep(2000);
                log.info(Thread.currentThread().getName()+"我后来做的事完成啦！");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public static void main(String[] args) {
            int threadSum = 4;
            CountDownLatch countDownLatch = new CountDownLatch(threadSum);

            for (int i=0;i<threadSum;i++) {
                new CountDownLatchTest.Writer(countDownLatch).start();
            }
            try {
                countDownLatch.await();// 等待所有线程执行完成countDown之前的操作
                log.info("所有线程执行完成。你可以做下一步操作啦~~~");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
