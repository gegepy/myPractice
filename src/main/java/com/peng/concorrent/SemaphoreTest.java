package com.peng.concorrent;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * @Auther: pengyan
 * @Date: 2018/6/25 09:26
 * @Description: 信号量 Semaphore 同时执行最大线程数
 */
public class SemaphoreTest {

    private static final int TIMES_COUNT = 10000;

    private static final int THREAD_COUNT = 50;

    private static int count = 0;

    public static void main(String[] args) throws InterruptedException {
        // 系统推荐线程数
//        System.out.println(Runtime.getRuntime().availableProcessors());

        Semaphore semaphore = new Semaphore(THREAD_COUNT);
        CountDownLatch countDownLatch = new CountDownLatch(TIMES_COUNT);
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < TIMES_COUNT; i++) {
            executorService.execute(() -> {
                try {
                    semaphore.acquire();
                    add();
                    semaphore.release();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    countDownLatch.countDown();
                }
            });
        }
        countDownLatch.await();
        executorService.shutdown();
        System.out.println("count：" + count);
    }

    private static void add() {
        count++;
    }
}
