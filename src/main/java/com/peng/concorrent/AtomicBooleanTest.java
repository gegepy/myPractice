package com.peng.concorrent;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 可以用于多线程下只让某段代码执行一次
 *
 * @author <a href="mailto:pengyan@loonxi.com">pengyan</a>
 * @date 2018/8/1 20:44
 **/
public class AtomicBooleanTest {

    // 总线程数
    private final static int clientTotal = 5000;
    // 同时执行线程数
    private final static int threadTotal = 200;

    private static AtomicBoolean isHappened = new AtomicBoolean(false);

    public static void main(String[] args) throws Exception {
        ExecutorService executors = Executors.newCachedThreadPool();
        Semaphore semaphore = new Semaphore(threadTotal);
        CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        for (int i=0;i<clientTotal;i++) {
            executors.execute(() -> {
                try {
                    semaphore.acquire();
                    test();
                    semaphore.release();
                } catch (Exception e) {

                } finally {
                    countDownLatch.countDown();
                }
            });
        }
        countDownLatch.await();
        executors.shutdown();
        System.out.println("结果："+isHappened.get());
    }

    private static void test() {
        if(isHappened.compareAndSet(false, true)) {
            System.out.println("我是不是只出现一次~");
        }
    }
}
