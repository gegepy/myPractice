package com.peng.sort;

import java.util.Arrays;

/**
 * 快速排序实现
 */
public class QuickSort {

    /**
     * 给区间内的元素快速排序
     *
     * @param array
     * @param left
     * @param right
     */
    public static void quickSort(int[] array, int left, int right) {
        if (left < right) {
            int pos = partition(array, left, right);
            quickSort(array, left, pos - 1);
            quickSort(array, pos + 1, right);
        }
    }

    /**
     * 快速根据基准值排序
     *
     * @param array
     * @param left
     * @param right
     * @return 返回基准值最终下标
     */
    private static int partition(int[] array, int left, int right) {
        int key = right;

        while (left < right) {
            while (left < right && array[left] <= array[key]) {
                left++;
            }
            while (left < right && array[right] >= array[key]) {
                right--;
            }
            swap(array, left, right);
        }
        swap(array, left, key);
        return left;
    }

    /**
     * 交换数值
     *
     * @param array
     * @param a
     * @param b
     */
    private static void swap(int[] array, int a, int b) {
        int t = array[a];
        array[a] = array[b];
        array[b] = t;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{9,8,10,4,7,2,5};
        System.out.println(Arrays.toString(arr));
        quickSort(arr, 0, arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }
}
